using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Repositories.Settings;
using MongoDB.Driver;
using OrderService.Model;
using OrderService.Repositories.Interfaces;

namespace OrderService.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IMongoCollection<Order> _order;
        public OrderRepository(IMongoSettings settings)
        {
            var client = new MongoClient(settings.Server);
            var database = client.GetDatabase(settings.Database);
            _order = database.GetCollection<Order>(settings.OrderCollection);
        }

        // Approwed,Rejected,Declined,Waiting
        public async Task<bool> ChangeStatus(Guid id, string status)
        {
            var filter = Builders<Order>.Filter.Eq(o => o.Id, id);
            var update = Builders<Order>.Update
                .Set(o => o.Status, status)
                .Set(o => o.UpdatedAt, DateTime.Now);
            var result = await _order.UpdateOneAsync(filter,update);

            return result.ModifiedCount > 0 ? true : false;
        }

        public async Task<Guid> Create(Order entity)
        {
            entity.Product.Id = Guid.NewGuid();
            entity.CreatedAt = DateTime.Now;
            entity.Status = "waiting";
            await _order.InsertOneAsync(entity);
            return entity.Id;
        }

        public async Task<bool> CreateMany(List<Order> orders)
        {
            foreach(var order in orders){
                order.CreatedAt = DateTime.Now;
                order.Status = "waiting";
                order.Product.Id = Guid.NewGuid();
            }
            await _order.InsertManyAsync(orders);

            return orders.Count() > 0 ? true : false;
        }

        public async Task<bool> Delete(Guid id)
        {
            var result = await _order.DeleteOneAsync(o => o.Id == id);
            return result.DeletedCount == 1 ? true : false;
        }

        public async Task<Order> Get(Guid id)
        {
            var result = await _order.FindAsync(o => o.Id == id && o.isDeleted == false);
            return result.FirstOrDefault();
        }

        public async Task<ICollection<Order>> GetAll()
        {
            return await _order.Find(o => true).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetAllBySoftDeleted()
        {
            return await _order.Find(o => o.isDeleted == true).ToListAsync();
        }

        public async Task<IEnumerable<Order>> GetMany(List<Guid> ids)
        {
            var filter = Builders<Order>.Filter.In(o => o.Id, ids);
            var result = await _order.Find(filter).ToListAsync();
            return result;
        }

        public async Task<bool> SoftDelete(Guid id)
        {
            var filter = Builders<Order>.Filter.Eq(p => p.Id, id);
            var update = Builders<Order>.Update
                .Set(p => p.isDeleted, true);
            var result = await _order.UpdateOneAsync(filter,update);
            return result.ModifiedCount == 1 ? true : false;
        }

        public async Task<bool> Update(Guid id, Order entity)
        {
            var filter = Builders<Order>.Filter.Eq(o => o.Id, id);
            var update = Builders<Order>.Update
                .Set(o => o.Quantity, entity.Quantity)
                .Set(o => o.Price, entity.Price)
                .Set(o => o.Status, entity.Status)
                .Set(o => o.isDeleted, entity.isDeleted)
                .Set(o => o.UpdatedAt, DateTime.Now)

                .Set(o => o.CustomerId, entity.CustomerId)
                .Set(o => o.Address.AddressLine, entity.Address.AddressLine)
                .Set(o => o.Address.City, entity.Address.City)
                .Set(o => o.Address.Country, entity.Address.Country)
                .Set(o => o.Address.CityCode, entity.Address.CityCode)

                .Set(o => o.Product.ImageUrl, entity.Product.ImageUrl)
                .Set(o => o.Product.Name, entity.Product.Name);
            var result = await _order.UpdateOneAsync(filter,update);
            //var result = await _order.ReplaceOneAsync(o => o.Id == id, entity);

            return result.ModifiedCount > 0 ? true : false;
        }
    }
}