using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model;

namespace OrderService.Model.Dtos.Responses
{
    public class OrderResponse : Document
    {
        public Guid CustomerId { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public string Status { get; set; }
        public ProductResponse Product { get; set; }
        public AddressResponse Address { get; set; }
    }
}