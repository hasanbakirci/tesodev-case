using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderService.Clients
{
    public class ClientSettings
    {
        public static string baseUrl = "https://localhost:5001/";
        public static string isValidateUrl = baseUrl+"api/Customers/Validate/";
    }
}