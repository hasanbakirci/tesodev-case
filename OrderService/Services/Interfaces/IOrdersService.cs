using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.ServerResponse;
using OrderService.Model.Dtos.Requests;
using OrderService.Model.Dtos.Responses;

namespace OrderService.Services.Interfaces
{
    public interface IOrdersService
    { 
        Task<Response<IEnumerable<OrderResponse>>> GetAll();
        Task<Response<OrderResponse>> GetById(Guid id);
        Task<Response<IEnumerable<OrderResponse>>> GetMany(List<Guid> ids);
        Task<Response<Guid>> Create(CreateOrderRequest request);
        Task<Response<bool>> Update(Guid id, UpdateOrderRequest request);
        Task<Response<bool>> Delete(Guid id);
        Task<Response<bool>> ChangeStatus(Guid id, ChangeStatusOrderRequest request);
        Task<Response<bool>> SoftDelete(Guid id);
        Task<Response<IEnumerable<OrderResponse>>> GetAllBySoftDeleted();
        Task<Response<bool>> CreateMany(List<CreateOrderRequest> requests);
    }
}