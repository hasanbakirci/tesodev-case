using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model;
using OrderService.Model;
using OrderService.Model.Dtos.Requests;
using OrderService.Model.Dtos.Responses;

namespace OrderService.Extensions
{
    public static class ConverterExtensions
    {
        public static Order ConvertToOrder(this CreateOrderRequest request){
            return new Order
                {
                    CustomerId = request.CustomerId,
                    Price = request.Price,
                    Quantity = request.Quantity,
                    Product = new Product {
                        Name = request.CreateProductRequest.Name,
                        ImageUrl = request.CreateProductRequest.ImageUrl
                    },
                    Address = new Address {
                        AddressLine = request.CreateAddressRequest.AddressLine,
                        City = request.CreateAddressRequest.City,
                        Country = request.CreateAddressRequest.Country,
                        CityCode = request.CreateAddressRequest.CityCode
                    }
                };
        }

        public static List<Order> ConverToOrderList(this List<CreateOrderRequest> requests){
            List<Order> orders = new List<Order>();
            requests.ToList().ForEach(o => orders.Add(
                new Order
                    {
                        Quantity = o.Quantity,
                        Price = o.Price,
                        CustomerId = o.CustomerId,
                        Product = new Product 
                            {
                                ImageUrl = o.CreateProductRequest.ImageUrl,
                                Name = o.CreateProductRequest.Name
                            },
                        Address = new Address
                            {
                                AddressLine = o.CreateAddressRequest.AddressLine,
                                City = o.CreateAddressRequest.City,
                                Country = o.CreateAddressRequest.Country,
                                CityCode = o.CreateAddressRequest.CityCode,
                            }
                    }
            ));
            return orders;
        }


        public static IEnumerable<OrderResponse> ConvertToOrderListResponse(this IEnumerable<Order> orders){
            List<OrderResponse> orderResponses = new List<OrderResponse>();
            orders.ToList().ForEach(o => orderResponses.Add(
                new OrderResponse
                    {
                        Id = o.Id,
                        CustomerId = o.CustomerId,
                        Quantity = o.Quantity,
                        Price = o.Price,
                        Status = o.Status,
                        CreatedAt = o.CreatedAt,
                        UpdatedAt = o.UpdatedAt,
                        isDeleted = o.isDeleted,
                        Address = new AddressResponse 
                            {
                                AddressLine = o.Address.AddressLine,
                                City = o.Address.City,
                                Country = o.Address.Country,
                                CityCode = o.Address.CityCode
                            },
                        Product = new ProductResponse
                            {
                                Id = o.Product.Id,
                                ImageUrl = o.Product.ImageUrl,
                                Name = o.Product.Name
                            }
                    }
            ));
            return orderResponses;
        }

        public static OrderResponse ConvertToOrderResponse(this Order order){
            return new OrderResponse
                {
                    Id = order.Id,
                        CustomerId = order.CustomerId,
                        Quantity = order.Quantity,
                        Price = order.Price,
                        Status = order.Status,
                        CreatedAt = order.CreatedAt,
                        UpdatedAt = order.UpdatedAt,
                        isDeleted = order.isDeleted,
                        Address = new AddressResponse 
                            {
                                AddressLine = order.Address.AddressLine,
                                City = order.Address.City,
                                Country = order.Address.Country,
                                CityCode = order.Address.CityCode
                            },
                        Product = new ProductResponse
                            {
                                Id = order.Product.Id,
                                ImageUrl = order.Product.ImageUrl,
                                Name = order.Product.Name
                            }
                };
        }


        public static Order ConvertToOrder(this UpdateOrderRequest request, Guid id){
            return new Order
                {
                    Id = id,
                    CustomerId = request.CustomerId,
                    Quantity = request.Quantity,
                    Price = request.Price,
                    isDeleted = request.isDeleted,
                    Product = new Product
                        {
                            ImageUrl = request.UpdateProductRequest.ImageUrl,
                            Name = request.UpdateProductRequest.Name
                        }
                };
        }

        public static Address ConvertToAdress(this AddressResponse address){
            return new Address
                {
                    AddressLine = address.AddressLine,
                    City = address.City,
                    Country = address.Country,
                    CityCode = address.CityCode
                };
        }


        // public static Product ConvertToProduct(this CreateProductRequest request){
        //     return new Product
        //         {
        //             ImageUrl = request.ImageUrl,
        //             Name = request.Name
        //         };
        // }

        // public static List<Product> ConvertToProductList(this List<CreateProductRequest> request){
        //     List<Product> products = new List<Product>();
        //     request.ToList().ForEach(p => products.Add(
        //         new Product
        //         {
        //             ImageUrl = p.ImageUrl,
        //             Name = p.Name,
        //         }
        //     ));
        //     return products;
        // }

        // public static IEnumerable<ProductResponse> ConvertToProductListResponse(this IEnumerable<Product> products){
        //     List<ProductResponse> productResponses = new List<ProductResponse>();
        //     products.ToList().ForEach(p => productResponses.Add(
        //         new ProductResponse
        //             {
        //                 Id = p.Id,
        //                 ImageUrl = p.ImageUrl,
        //                 Name = p.Name,
        //             }
        //     ));
        //     return productResponses;
        // }


        // public static ProductResponse ConvertToProductResponse(this Product product){
        //     return new ProductResponse
        //         {
        //             Id = product.Id,
        //             ImageUrl = product.ImageUrl,
        //             Name = product.Name
        //         };
        // }

    }
}