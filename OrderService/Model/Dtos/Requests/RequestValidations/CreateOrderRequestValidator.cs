using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace OrderService.Model.Dtos.Requests.RequestValidations
{
    public class CreateOrderRequestValidator : AbstractValidator<CreateOrderRequest>
    {
        public CreateOrderRequestValidator()
        {
            RuleFor(r => r.CustomerId).NotEmpty();
            RuleFor(r => r.Price).GreaterThan(0).NotEmpty();
            RuleFor(r => r.Quantity).GreaterThan(0).NotEmpty();

            RuleFor(r => r.CreateProductRequest.Name).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.CreateProductRequest.ImageUrl).MinimumLength(2).MaximumLength(150).NotEmpty();

            RuleFor(r => r.CreateAddressRequest.AddressLine).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.CreateAddressRequest.City).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.CreateAddressRequest.Country).MinimumLength(2).MaximumLength(150).NotEmpty();
            RuleFor(r => r.CreateAddressRequest.CityCode).GreaterThan(0).NotEmpty();

        }
    }
}