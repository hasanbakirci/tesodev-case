using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Repositories.Settings
{
    public interface IMongoSettings
    {
        string Server { get; set; }
        string Database { get; set; }
        string CustomerCollection { get; set; }
        string ProductCollection { get; set; }
        string OrderCollection { get; set; }
    }
}