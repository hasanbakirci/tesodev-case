using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace OrderService.Model.Dtos.Requests.RequestValidations
{
    public class ChangeStatusOrderRequestValidator : AbstractValidator<ChangeStatusOrderRequest>
    {
        public ChangeStatusOrderRequestValidator()
        {
            RuleFor(r => r.Status).Must(ChooseStatus).WithMessage("Only approwed, rejected, declined, waiting values can be entered").NotEmpty();
        }

        private bool ChooseStatus(string arg){
            string[] statusList = {"approwed","rejected","declined","waiting"};
            var result = Array.Exists(statusList,arg.Contains);
            return result;
        }
    }
}