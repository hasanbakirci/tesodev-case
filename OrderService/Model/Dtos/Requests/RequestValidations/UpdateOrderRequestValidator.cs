using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation;

namespace OrderService.Model.Dtos.Requests.RequestValidations
{
    public class UpdateOrderRequestValidator : AbstractValidator<UpdateOrderRequest>
    {
        public UpdateOrderRequestValidator()
        {
            RuleFor(r => r.Price).GreaterThan(0);
            RuleFor(r => r.Quantity).GreaterThan(0);
            RuleFor(r => r.UpdateProductRequest.ImageUrl).MinimumLength(3).MaximumLength(30);
            RuleFor(r => r.UpdateProductRequest.Name).MinimumLength(3).MaximumLength(30);
            RuleFor(r => r.Status).Must(ChooseStatus).WithMessage("Only approwed, rejected, declined, waiting values can be entered");
        }
        private bool ChooseStatus(string arg){
            string[] statusList = {"approwed","rejected","declined","waiting"};
            var result = Array.Exists(statusList,arg.Contains);
            return result;
        }
    }
}