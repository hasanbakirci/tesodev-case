using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Core.ServerResponse;
using Newtonsoft.Json;
using OrderService.Model.Dtos.Responses;

namespace OrderService.Clients
{
    public class CustomerClient
    {
        public async Task<Response<bool>> CustomerIsValid(Guid id){
            using(var client = new HttpClient())
            {
                HttpResponseMessage response = await client.GetAsync(ClientSettings.isValidateUrl+id);
                if(response.StatusCode == HttpStatusCode.OK){
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();
                    var result = JsonConvert.DeserializeObject<CustomerApiResponse>(responseBody);
                    return new SuccessResponse<bool>(result.Data);
                }
            }
            throw new InvalidOperationException("Customer validation error.");
        }
    }
}