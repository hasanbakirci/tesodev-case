using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrderService.Model;

namespace OrderService.Repositories.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<bool> ChangeStatus(Guid id, string status);
        Task<bool> SoftDelete(Guid id);
        Task<IEnumerable<Order>> GetAllBySoftDeleted();
        Task<IEnumerable<Order>> GetMany(List<Guid> ids);
        Task<bool> CreateMany(List<Order> orders);
    }
}