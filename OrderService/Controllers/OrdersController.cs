using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Api;
using Microsoft.AspNetCore.Mvc;
using OrderService.Model.Dtos.Requests;
using OrderService.Services.Interfaces;

namespace OrderService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrdersController : ApiController
    {
        private readonly IOrdersService _ordersService;

        public OrdersController(IOrdersService ordersService)
        {
            _ordersService = ordersService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll(){
            var orders = await _ordersService.GetAll();
            return ApiResponse(orders);
        }

        [HttpGet("Search/{id}")]
        public async Task<IActionResult> GetById(Guid id){
            var order = await _ordersService.GetById(id);
            return ApiResponse(order);
        }

        [HttpGet("SearchMany")]
        public async Task<IActionResult> GetMany([FromQuery] List<Guid> ids){
            var orders = await _ordersService.GetMany(ids);
            return ApiResponse(orders);
        }

        [HttpGet("GetAllBySoftDeleted")]
        public async Task<IActionResult> GetAllBySoftDeleted(){
            var orders = await _ordersService.GetAllBySoftDeleted();
            return ApiResponse(orders);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateOrderRequest request){
            var result = await _ordersService.Create(request);
            return ApiResponse(result);
        }

        [HttpPost("CreateMany")]
        public async Task<IActionResult> CreateMany(List<CreateOrderRequest> requests){
            var result = await _ordersService.CreateMany(requests);
            return ApiResponse(result);
        }

        [HttpPut("Update/{id}")]
        public async Task<IActionResult> Update(Guid id, [FromBody] UpdateOrderRequest request){
            var result = await _ordersService.Update(id,request);
            return ApiResponse(result);
        }

        [HttpPut("ChangeStatus/{id}")]
        public async Task<IActionResult> ChangeStatus(Guid id, ChangeStatusOrderRequest request){
            var result = await _ordersService.ChangeStatus(id,request);
            return ApiResponse(result);
        }

        [HttpDelete("Delete/{id}")]
        public async Task<IActionResult> Delete(Guid id){
            var result = await _ordersService.Delete(id);
            return ApiResponse(result);
        }

        [HttpPut("SoftDelete/{id}")]
        public async Task<IActionResult> SoftDelete(Guid id){
            var result = await _ordersService.SoftDelete(id);
            return ApiResponse(result);
        }

    }
}