using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.ServerResponse;
using Microsoft.AspNetCore.Mvc;

namespace Core.Api
{
    [ApiController]
    [Route("api/[controller]")]
    public class ApiController : ControllerBase
    {
        [NonAction]
        protected IActionResult ApiResponse(Response response) => StatusCode(response.StatusCode, response);

        [NonAction]
        protected IActionResult ApiResponse<T>(Response<T> response) => StatusCode(response.StatusCode, response);
    }
}